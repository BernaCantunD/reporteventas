/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menus;

import Clases.conectar;
import de.javasoft.plaf.synthetica.SyntheticaBlackEyeLookAndFeel;
import de.javasoft.plaf.synthetica.SyntheticaRootPaneUI;
import java.awt.CardLayout;
import java.sql.Connection;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import sun.security.action.PutAllAction;

/**
 *
 * @author PC_Cantun
 */

public class MENU extends javax.swing.JFrame {
    
    /**
     * Creates new form MENU
     */
    boolean Ventas, Productos, Configuracion, Corte, Inventario, Administrador;
    CardLayout card;
    conectar con = new conectar();
   Connection cn = con.conexion();
    private Object st;
    
    
    public MENU() {
        initComponents();
        setTitle("MENU ADMINISTRADOR");
        setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("../Img/My Computer.png")).getImage());
        Ventas = Productos = Configuracion = Corte = Inventario = Administrador = false;
       card = (CardLayout) panel_pestañas.getLayout();    
     
       
    }
    


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel_contenido = new javax.swing.JPanel();
        Panel_Menus = new javax.swing.JPanel();
        btn_cerrar = new javax.swing.JButton();
        btn_Ventas = new javax.swing.JButton();
        btn_Configuracion = new javax.swing.JButton();
        btn_Inventario = new javax.swing.JButton();
        btn_Productos = new javax.swing.JButton();
        Exit = new javax.swing.JButton();
        Panel_Nombre = new javax.swing.JPanel();
        lblusuario1 = new javax.swing.JLabel();
        lblusuario = new javax.swing.JLabel();
        cLabel1 = new com.bolivia.label.CLabel();
        btnexit = new javax.swing.JButton();
        btnmenu = new javax.swing.JButton();
        panel_pestañas = new javax.swing.JPanel();
        panel_pestañas1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1250, 700));
        setUndecorated(true);

        panel_contenido.setBackground(new java.awt.Color(255, 255, 255));
        panel_contenido.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 5));
        panel_contenido.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Panel_Menus.setBackground(new java.awt.Color(255, 255, 255));
        Panel_Menus.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        Panel_Menus.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_cerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/regresar.png"))); // NOI18N
        btn_cerrar.setBorder(null);
        btn_cerrar.setBorderPainted(false);
        btn_cerrar.setContentAreaFilled(false);
        btn_cerrar.setFocusPainted(false);
        btn_cerrar.setFocusable(false);
        btn_cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cerrarActionPerformed(evt);
            }
        });
        Panel_Menus.add(btn_cerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 580, -1, -1));

        btn_Ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/ventas.png"))); // NOI18N
        btn_Ventas.setAutoscrolls(true);
        btn_Ventas.setBorder(null);
        btn_Ventas.setBorderPainted(false);
        btn_Ventas.setContentAreaFilled(false);
        btn_Ventas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_Ventas.setFocusPainted(false);
        btn_Ventas.setFocusable(false);
        btn_Ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_VentasActionPerformed(evt);
            }
        });
        Panel_Menus.add(btn_Ventas, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, -1, -1));

        btn_Configuracion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/configurar.png"))); // NOI18N
        btn_Configuracion.setBorder(null);
        btn_Configuracion.setBorderPainted(false);
        btn_Configuracion.setContentAreaFilled(false);
        btn_Configuracion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_Configuracion.setFocusPainted(false);
        btn_Configuracion.setFocusable(false);
        btn_Configuracion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ConfiguracionActionPerformed(evt);
            }
        });
        Panel_Menus.add(btn_Configuracion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 420, -1, -1));

        btn_Inventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/inventario.png"))); // NOI18N
        btn_Inventario.setBorder(null);
        btn_Inventario.setBorderPainted(false);
        btn_Inventario.setContentAreaFilled(false);
        btn_Inventario.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_Inventario.setFocusPainted(false);
        btn_Inventario.setFocusable(false);
        btn_Inventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_InventarioActionPerformed(evt);
            }
        });
        Panel_Menus.add(btn_Inventario, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 270, -1, -1));

        btn_Productos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/productos.png"))); // NOI18N
        btn_Productos.setBorder(null);
        btn_Productos.setBorderPainted(false);
        btn_Productos.setContentAreaFilled(false);
        btn_Productos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_Productos.setFocusPainted(false);
        btn_Productos.setFocusable(false);
        btn_Productos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ProductosActionPerformed(evt);
            }
        });
        Panel_Menus.add(btn_Productos, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 340, -1, -1));

        Exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/salir.png"))); // NOI18N
        Exit.setBorder(null);
        Exit.setBorderPainted(false);
        Exit.setContentAreaFilled(false);
        Exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExitActionPerformed(evt);
            }
        });
        Panel_Menus.add(Exit, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 630, -1, -1));

        Panel_Nombre.setBackground(new java.awt.Color(255, 255, 255));
        Panel_Nombre.setForeground(new java.awt.Color(87, 24, 69));
        Panel_Nombre.add(lblusuario1);
        Panel_Nombre.add(lblusuario);

        Panel_Menus.add(Panel_Nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 260, 20));

        cLabel1.setBackground(new java.awt.Color(255, 255, 255));
        cLabel1.setBorder(null);
        cLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/icono-1 - copia.png"))); // NOI18N
        cLabel1.setText("");
        cLabel1.setFocusable(false);
        Panel_Menus.add(cLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 20, 180, 180));

        panel_contenido.add(Panel_Menus, new org.netbeans.lib.awtextra.AbsoluteConstraints(-265, 5, 260, 690));

        btnexit.setBackground(new java.awt.Color(0, 153, 204));
        btnexit.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnexit.setForeground(new java.awt.Color(255, 255, 255));
        btnexit.setText("X");
        btnexit.setBorder(null);
        btnexit.setContentAreaFilled(false);
        btnexit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnexit.setOpaque(true);
        btnexit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnexitActionPerformed(evt);
            }
        });
        panel_contenido.add(btnexit, new org.netbeans.lib.awtextra.AbsoluteConstraints(1190, 10, 50, 50));

        btnmenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/icono-menu.png"))); // NOI18N
        btnmenu.setBorder(null);
        btnmenu.setContentAreaFilled(false);
        btnmenu.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnmenu.setFocusPainted(false);
        btnmenu.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/icono-menu1.png"))); // NOI18N
        btnmenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmenuActionPerformed(evt);
            }
        });
        panel_contenido.add(btnmenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 5, 60, 50));

        panel_pestañas.setBackground(new java.awt.Color(255, 255, 255));
        panel_pestañas.setLayout(new java.awt.CardLayout());

        panel_pestañas1.setBackground(new java.awt.Color(255, 255, 255));
        panel_pestañas1.setLayout(new java.awt.CardLayout());
        panel_pestañas.add(panel_pestañas1, "card2");

        panel_contenido.add(panel_pestañas, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 0, 1180, 700));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel_contenido, javax.swing.GroupLayout.PREFERRED_SIZE, 1250, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel_contenido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_VentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_VentasActionPerformed
        // TODO add your handling code here:
        mostrarVentas();
    }//GEN-LAST:event_btn_VentasActionPerformed

    private void btn_ProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ProductosActionPerformed
        // TODO add your handling code here:
        mostrarProductos();
    }//GEN-LAST:event_btn_ProductosActionPerformed

    private void btn_cerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cerrarActionPerformed
        Index ventana = new Index ();
        ventana.setVisible(true);
        ventana.pack();
        this.setVisible(false);
          
    }//GEN-LAST:event_btn_cerrarActionPerformed

    private void ExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExitActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_ExitActionPerformed

    private void btn_ConfiguracionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ConfiguracionActionPerformed
        // TODO add your handling code here:
        mostrarConfiguracion();
    }//GEN-LAST:event_btn_ConfiguracionActionPerformed

    private void btn_InventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_InventarioActionPerformed
        mostrarInventario();
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_InventarioActionPerformed

    private void btnmenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmenuActionPerformed
        int posicion = btnmenu.getX();
        if(posicion > 5) {
             Animacion.Animacion.mover_izquierda(263, 5, 2, 2, btnmenu);
             Animacion.Animacion.mover_izquierda(5, -265, 2, 2, Panel_Menus);
         }
        else {
            Animacion.Animacion.mover_derecha(5, 263, 2, 2, btnmenu);
            Animacion.Animacion.mover_derecha(-265, 5, 2,  2, Panel_Menus);
        }
    }//GEN-LAST:event_btnmenuActionPerformed

    private void btnexitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnexitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnexitActionPerformed
    
    
    public void mostrarVentas(){
     Panel_Ventas ventas = new Panel_Ventas ();
     panel_pestañas.add(ventas, "Ventas");
     card.show(panel_pestañas, "Ventas");
     Ventas = true;
     Configuracion = Inventario = Corte = Productos = Administrador= false;
     btn_Configuracion.setLocation(-175, btn_Configuracion.getY());
     btn_Inventario.setLocation(-175, btn_Inventario.getY());
     btn_Productos.setLocation(-175, btn_Productos.getY());
     btn_Ventas.setLocation(0, btn_Ventas.getY());
     SwingUtilities.updateComponentTreeUI(this);   
    }
    /**
     * @param args the command line arguments
     */
    public void mostrarProductos(){
        Panel_Productos productos = new Panel_Productos();
        panel_pestañas.add(productos, "Productos");
        card.show(panel_pestañas, "Productos");
        Productos = true;
        Configuracion = Inventario = Corte = Ventas = Administrador = false;
     btn_Configuracion.setLocation(-175, btn_Configuracion.getY());
     btn_Inventario.setLocation(-175, btn_Inventario.getY());
     btn_Ventas.setLocation(-175,  btn_Ventas.getY());
     btn_Productos.setLocation(0, btn_Productos.getY());
     SwingUtilities.updateComponentTreeUI(this); 
    }
    
    public void mostrarAdministrador (){
        Panel_Administrador administrador = new Panel_Administrador();
       panel_pestañas.add(administrador, "Administrador");
       card.show(panel_pestañas, "Administrador");
       Administrador = true; 
       Configuracion = Inventario = Corte = Ventas = Administrador = false;
     btn_Configuracion.setLocation(-175, btn_Configuracion.getY());
     btn_Inventario.setLocation(-175, btn_Inventario.getY());
     btn_Ventas.setLocation(-175,  btn_Ventas.getY());
     btn_Productos.setLocation(-175, btn_Productos.getY());
     SwingUtilities.updateComponentTreeUI(this);
    }
    public void mostrarConfiguracion(){
        Panel_Configuracion configuracion = new Panel_Configuracion();
       panel_pestañas.add(configuracion , "Configuracion");
       card.show(panel_pestañas, "Configuracion");
       Configuracion = true; 
       Configuracion = Inventario = Corte = Ventas = Administrador = false;
     btn_Configuracion.setLocation(0, btn_Configuracion.getY());
     btn_Inventario.setLocation(-175, btn_Inventario.getY());
     btn_Ventas.setLocation(-175,  btn_Ventas.getY());
     btn_Productos.setLocation(-175, btn_Productos.getY());
     SwingUtilities.updateComponentTreeUI(this);
    }
            
    public void mostrarInventario(){
        Panel_Inventario inventario = new Panel_Inventario();
       panel_pestañas.add(inventario , "Configuracion");
       card.show(panel_pestañas, "Configuracion");
       Configuracion = true; 
       Configuracion = Inventario = Corte = Ventas = Administrador = false;
     btn_Configuracion.setLocation(-175, btn_Configuracion.getY());
     btn_Inventario.setLocation(0, btn_Inventario.getY());
     btn_Ventas.setLocation(-175,  btn_Ventas.getY());
     btn_Productos.setLocation(-175, btn_Productos.getY());
     SwingUtilities.updateComponentTreeUI(this);
    }
    
            
    public static void main(String args[]) {
       
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MENU.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MENU.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MENU.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MENU.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (Exception e) {
                }
                new MENU().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Exit;
    private javax.swing.JPanel Panel_Menus;
    private javax.swing.JPanel Panel_Nombre;
    private javax.swing.JButton btn_Configuracion;
    private javax.swing.JButton btn_Inventario;
    private javax.swing.JButton btn_Productos;
    private javax.swing.JButton btn_Ventas;
    private javax.swing.JButton btn_cerrar;
    private javax.swing.JButton btnexit;
    private javax.swing.JButton btnmenu;
    private com.bolivia.label.CLabel cLabel1;
    public static javax.swing.JLabel lblusuario;
    public static javax.swing.JLabel lblusuario1;
    private javax.swing.JPanel panel_contenido;
    private javax.swing.JPanel panel_pestañas;
    private javax.swing.JPanel panel_pestañas1;
    // End of variables declaration//GEN-END:variables

    void lblusuario(String usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
