/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menus;

import Clases.GenerarCodigo;
import Clases.conectar;
import java.awt.Image;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author PC_Cantun
 */
public final class Panel_Administrador extends javax.swing.JPanel {
   conectar con = new conectar();
   Connection cn = con.conexion();
    private Object st;
    
 
    /**
     * Creates new form Panel_Admin
     */
    public String id;

    public Panel_Administrador() {
        initComponents();
        mostrartabla("");
        bloquear();
        cargarcodigo();
                }
    
     void cargarcodigo()
    {
    
       int j;
       String num="";
       String c = "";
       String SQL="select max(numero) from usuario";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            if(rs.next())
            {
                c=rs.getString(1);
            }
               
            if(c==null)
            {
                codigo.setText("US0001");
            }else{
                char r1=c.charAt(2);
                char r2=c.charAt(3);
                char r3=c.charAt(4);
                char r4=c.charAt(5);
                String r ="";
                r = "" +r1+r2+r3+r4;
                
                
                j=Integer.parseInt(r);
                GenerarCodigo gen = new GenerarCodigo();
                gen.generar(j);
                codigo.setText("US"+gen.serie());
                
            }
            
        } catch (Exception e) {
        }
    }
     void bloquear(){
 //   txtID.setEnabled(false);
    codigo.setEnabled(false);
    txtnick.setEnabled(false);
    txtnombre.setEnabled(false);
    txtpassword.setEnabled(false);
    txtedad.setEnabled(false);
    txtcorreo.setEnabled(false);
   // txt.setEnabled(false);
    //txtruc.setEnabled(false);
    //txtdni.setEnabled(false);
    cmb_tipousuario.setEnabled(false);
    btn_guardar.setEnabled(false);
    btnseleccionar.setEnabled(false);
    btncancelar.setEnabled(false);
    btnactualizar.setEnabled(false);
    }
     void limpiar(){
    codigo.setText("");
    //txtID.setText("");
    txtnick.setText("");
    txtnombre.setText("");
    txtpassword.setText("");
    txtedad.setText("");
    txtcorreo.setText("");
//    cmb_tipousuario.setText("");
    btn_guardar.setText("");
    lblimagen.setIcon(null);
    
    }
      void desbloquear(){
    codigo.setEnabled(true);
    txtnick.setEnabled(true);
    txtnombre.setEnabled(true);
    txtpassword.setEnabled(true);
    txtedad.setEnabled(true);
    txtcorreo.setEnabled(true);
   // txt.setEnabled(false);
    //txtruc.setEnabled(false);
    //txtdni.setEnabled(false);
    cmb_tipousuario.setEnabled(true);
    btn_guardar.setEnabled(true);
    //btnnuevo.setEnabled(true);
    btncancelar.setEnabled(true);
    btnactualizar.setEnabled(true);
    btnseleccionar.setEnabled(true);
    }
    
     void mostrartabla(String valor){
         
        DefaultTableModel modelo = new DefaultTableModel();
    modelo.addColumn("ID/Numero");
    modelo.addColumn("Nombre");
    modelo.addColumn("Edad");
    modelo.addColumn("Correo");
    modelo.addColumn("Nick");
    modelo.addColumn("Password");
    modelo.addColumn("Tipo de usuario");
    modelo.addColumn("Foto");
     tabladatos.setModel(modelo);
     
  
       String sql= "SELECT * FROM usuario";
       
       String datos[] = new String[7];
       Statement st;
       try {
           st = cn.createStatement();
           ResultSet rs = st.executeQuery(sql);
          while (rs.next()){
          datos[0]=rs.getString(1);
          datos[1]=rs.getString(2);
          datos[2]=rs.getString(3);
          datos[3]=rs.getString(4);
          datos[4]=rs.getString(5);
          datos[5]=rs.getString(6);
          datos[6]=rs.getString(7);
         // datos[7]=rs.getB
          modelo.addRow(datos);
          }
          tabladatos.setModel(modelo);          
       } catch (SQLException ex) {
           Logger.getLogger(Panel_Administrador.class.getName()).log(Level.SEVERE, null, ex);
       } 
         
     }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        nombre = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        usuario = new javax.swing.JLabel();
        contraseña = new javax.swing.JLabel();
        txtnick = new javax.swing.JTextField();
        txtpassword = new javax.swing.JPasswordField();
        btn_guardar = new javax.swing.JButton();
        tipousuario = new javax.swing.JLabel();
        cmb_tipousuario = new javax.swing.JComboBox();
        ID = new javax.swing.JLabel();
        btn_nuevo = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();
        btnactualizar = new javax.swing.JButton();
        edad = new javax.swing.JLabel();
        txtedad = new javax.swing.JTextField();
        correo = new javax.swing.JLabel();
        txtcorreo = new javax.swing.JTextField();
        codigo = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabladatos = new javax.swing.JTable();
        lblimagen = new com.bolivia.label.CLabel();
        btnseleccionar = new javax.swing.JButton();

        jMenuItem1.setText("Modificar");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);

        jMenuItem2.setText("Eliminar");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem2);

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(93, 173, 226));

        jLabel2.setFont(new java.awt.Font("Sylfaen", 1, 18)); // NOI18N
        jLabel2.setText("REGISTRAR USUARIOS");

        nombre.setText(" NOMBRE:");

        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnombreKeyTyped(evt);
            }
        });

        usuario.setText("USUARIO:");

        contraseña.setText("CONTRASEÑA:");

        btn_guardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/agregar.png"))); // NOI18N
        btn_guardar.setBorder(null);
        btn_guardar.setBorderPainted(false);
        btn_guardar.setContentAreaFilled(false);
        btn_guardar.setFocusable(false);
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });

        tipousuario.setText("TIPO DE USUARIO:");

        cmb_tipousuario.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Administrador", "Vendedor" }));

        ID.setText("ID:");

        btn_nuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/nuevo.png"))); // NOI18N
        btn_nuevo.setBorder(null);
        btn_nuevo.setBorderPainted(false);
        btn_nuevo.setContentAreaFilled(false);
        btn_nuevo.setFocusPainted(false);
        btn_nuevo.setFocusable(false);
        btn_nuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_nuevoActionPerformed(evt);
            }
        });

        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/cancelar.png"))); // NOI18N
        btncancelar.setBorder(null);
        btncancelar.setBorderPainted(false);
        btncancelar.setContentAreaFilled(false);
        btncancelar.setFocusPainted(false);
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        btnactualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/actualizar.png"))); // NOI18N
        btnactualizar.setBorder(null);
        btnactualizar.setBorderPainted(false);
        btnactualizar.setContentAreaFilled(false);
        btnactualizar.setFocusPainted(false);
        btnactualizar.setFocusable(false);
        btnactualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnactualizarActionPerformed(evt);
            }
        });

        edad.setText("Edad");

        txtedad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtedadKeyTyped(evt);
            }
        });

        correo.setText("Correo");

        codigo.setBackground(new java.awt.Color(51, 0, 153));
        codigo.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edad)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(57, 57, 57)
                                .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tipousuario)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmb_tipousuario, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btn_guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34)
                                .addComponent(btnactualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(ID, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(nombre))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(8, 8, 8)
                                        .addComponent(txtedad, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(75, 75, 75)
                                        .addComponent(correo, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(codigo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btn_nuevo))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(usuario)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtnick, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(48, 48, 48)
                                .addComponent(contraseña)
                                .addGap(18, 18, 18)
                                .addComponent(txtpassword, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(25, 25, 25))))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2)
                    .addGap(0, 258, Short.MAX_VALUE)))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(237, Short.MAX_VALUE)
                    .addComponent(txtcorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(30, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_nuevo)
                        .addGap(31, 31, 31))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(codigo, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ID))
                        .addGap(18, 18, 18)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nombre)
                    .addComponent(tipousuario)
                    .addComponent(cmb_tipousuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edad)
                    .addComponent(txtedad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(correo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(contraseña)
                    .addComponent(txtpassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtnick, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(usuario, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btncancelar)
                            .addComponent(btn_guardar)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(btnactualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(45, 45, 45))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 20, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 300, Short.MAX_VALUE)))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(165, Short.MAX_VALUE)
                    .addComponent(txtcorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(165, Short.MAX_VALUE)))
        );

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 480, 350));

        jPanel2.setBackground(new java.awt.Color(93, 173, 226));

        tabladatos.setBackground(new java.awt.Color(46, 134, 193));
        tabladatos.setForeground(new java.awt.Color(255, 255, 255));
        tabladatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabladatos.setComponentPopupMenu(jPopupMenu1);
        tabladatos.setGridColor(new java.awt.Color(255, 255, 255));
        tabladatos.setSelectionBackground(new java.awt.Color(210, 89, 50));
        jScrollPane1.setViewportView(tabladatos);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 890, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 380, 940, 280));

        lblimagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/icons8-Añadir usuario grupo mujer hombre_100 copia.png"))); // NOI18N
        lblimagen.setText("");
        lblimagen.setLineColor(new java.awt.Color(240, 240, 240));
        add(lblimagen, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 40, 300, 280));

        btnseleccionar.setText("Seleccionar Imagen");
        btnseleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnseleccionarActionPerformed(evt);
            }
        });
        add(btnseleccionar, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 340, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed
      
        String  cadena1, cadena2,cadena3,cadena4,cadena5,cadena6,cadena7;
        
        cadena1 = codigo.getText();
        cadena2 = txtnombre.getText();
        cadena3 = txtedad.getText();
        cadena4 = txtcorreo.getText();
        cadena5 = txtnick.getText();
        cadena6 = txtpassword.getText().toString();
        cadena7 = cmb_tipousuario.getSelectedItem().toString();

                if (codigo.getText().equals("") || txtnombre.getText().equals("") || (txtnick.getText().equals(""))
           || (txtpassword.getText().equals("")) || (cmb_tipousuario.getSelectedItem().equals(null))) {
            
            txtnombre.requestFocus();
        }
                
                        else {
        try {
           
            String url = "jdbc:mysql://localhost:3306/sistema";
            String usuario = "root";
            String contraseña = "";
            
             Class.forName("com.mysql.jdbc.Driver").newInstance(); 
            Connection con = DriverManager.getConnection(url,usuario,contraseña); 
             if ( con != null ) 
                    System.out.println("Se ha establecido una conexión a la base de datos " +  
                                       "\n " + url ); 
  
            Statement stmt = con.createStatement(); 
                  stmt.executeUpdate("INSERT INTO usuario VALUES('"+cadena1+"','"+cadena2+"','"+cadena3+"','"+cadena4+"','"+cadena5+"','"+cadena6+"','"+cadena7+"')");
                  System.out.println("Los valores han sido agregados a la base de datos ");
                 mostrartabla("");
                   
        } catch (InstantiationException ex) {
           Logger.getLogger(Panel_Administrador.class.getName()).log(Level.SEVERE, null, ex);
       } catch (IllegalAccessException ex) {
           Logger.getLogger(Panel_Administrador.class.getName()).log(Level.SEVERE, null, ex);
       } catch (ClassNotFoundException ex) {
           Logger.getLogger(Panel_Administrador.class.getName()).log(Level.SEVERE, null, ex);
       } catch (SQLException ex) {
           Logger.getLogger(Panel_Administrador.class.getName()).log(Level.SEVERE, null, ex);
       }
        
        finally {
            if (con != null) {
                try {
                    con.close();
                 //  stmt.close();
                } catch ( Exception e ) { 
                         System.out.println( e.getMessage()); 
                }
            }
        }
         javax.swing.JOptionPane.showMessageDialog(this,"Registro exitoso! \n","AVISO!",javax.swing.JOptionPane.INFORMATION_MESSAGE);
        }
        this.codigo.setText("");
        this.txtnombre.setText("");
        this.txtedad.setText("");
        this.txtcorreo.setText("");
        //this.txt_domicilio.setText("");
        //this.txt_telefono.setText("");
        this.txtnick.setText("");
        this.txtpassword.setText("");  
        cargarcodigo();
        bloquear();
        
    }//GEN-LAST:event_btn_guardarActionPerformed

    private void txtnombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyTyped
        char  c = evt.getKeyChar();
        if((c<'a' ||c>'z') && (c<'A' || c>'Z') && (c<' ' || c>' ')) evt.consume();
    }//GEN-LAST:event_txtnombreKeyTyped

    private void btn_nuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_nuevoActionPerformed
        // TODO add your handling code here:
        desbloquear();
    }//GEN-LAST:event_btn_nuevoActionPerformed

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        // TODO add your handling code here:
        limpiar();
        bloquear();
        
    }//GEN-LAST:event_btncancelarActionPerformed

    private void btnactualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnactualizarActionPerformed
       try {
           // TODO add your handling code here:
           PreparedStatement pst = cn.prepareStatement("UPDATE usuario SET numero='"+codigo.getText()+"',NOMBRE='"+txtnombre.getText()+"',edad='"+txtedad.getText()+"',correo='"+txtcorreo.getText()+"',nick='"+txtnick.getText()+"' ,password='"+txtpassword.getText()+"' ,tipousuario='"+cmb_tipousuario.getSelectedItem()+ "' WHERE numero='"+codigo.getText()+"'");
           pst.executeUpdate();
           mostrartabla("");
           bloquear();
       } catch (SQLException e) {
           System.out.print(e.getMessage());
           //Logger.getLogger(Panel_Administrador.class.getName()).log(Level.SEVERE, null, ex);
           JOptionPane.showMessageDialog(null, "No se han seleccionado datos");
       }
    }//GEN-LAST:event_btnactualizarActionPerformed
    
    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
    
         int fila = tabladatos.getSelectedRow();
        if(fila>=0){
          codigo.setText(tabladatos.getValueAt(fila, 0).toString());
          txtnombre.setText(tabladatos.getValueAt(fila, 1).toString());
          txtedad.setText(tabladatos.getValueAt(fila, 2).toString());
          txtcorreo.setText(tabladatos.getValueAt(fila, 3).toString());
          txtnick.setText(tabladatos.getValueAt(fila, 4).toString());
          txtpassword.setText(tabladatos.getValueAt(fila, 5).toString());
         cmb_tipousuario.getSelectedItem().equals(tabladatos.getValueAt(fila, 6).toString()); 
          desbloquear();
        }
        else{
        JOptionPane.showMessageDialog(null, "No se han seleccionado datos");
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        int fila = tabladatos.getSelectedRow();
        String cod="";
        cod=tabladatos.getValueAt(fila, 0).toString();
       try {
           PreparedStatement pst = cn.prepareStatement("DELETE FROM usuario WHERE numero='"+cod+"'");
           pst.executeUpdate();
           mostrartabla("");
           cargarcodigo();
       } catch (SQLException e) {          
       }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void txtedadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtedadKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtedadKeyTyped

    private void btnseleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnseleccionarActionPerformed
       JFileChooser Buscar=new JFileChooser();
    //   Buscar.showOpenDialog(this);
       FileNameExtensionFilter extension=new FileNameExtensionFilter("Seleccionar Imagen", "jpg","png","gif");
       Buscar.setFileFilter(extension);
       if (Buscar.showOpenDialog(this)==JFileChooser.APPROVE_OPTION){
       Toolkit tool=Toolkit.getDefaultToolkit();
       String ruta=Buscar.getSelectedFile().toString();
       Image imagen=tool.createImage(ruta);
    //   lblimagen.setIcon(new ImageIcon(imagen.getScaledInstance(lblimagen.getWidth(),lblimagen.getHeight(),Image.SCALE_AREA_AVERAGING)));
       lblimagen.setIcon(new ImageIcon(imagen.getScaledInstance(300,280,Image.SCALE_DEFAULT)));
       }
    }//GEN-LAST:event_btnseleccionarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ID;
    private javax.swing.JButton btn_guardar;
    private javax.swing.JButton btn_nuevo;
    private javax.swing.JButton btnactualizar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JButton btnseleccionar;
    private javax.swing.JComboBox cmb_tipousuario;
    private javax.swing.JLabel codigo;
    private javax.swing.JLabel contraseña;
    private javax.swing.JLabel correo;
    private javax.swing.JLabel edad;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private com.bolivia.label.CLabel lblimagen;
    private javax.swing.JLabel nombre;
    private javax.swing.JTable tabladatos;
    private javax.swing.JLabel tipousuario;
    private javax.swing.JTextField txtcorreo;
    private javax.swing.JTextField txtedad;
    private javax.swing.JTextField txtnick;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JPasswordField txtpassword;
    private javax.swing.JLabel usuario;
    // End of variables declaration//GEN-END:variables


    

 
}
